﻿// <copyright file="CarServiceTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Data;
    using CarService.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for logic test methods.
    /// </summary>
    [TestFixture]
    public class CarServiceTests
    {
        private Mock<IOwnerRepository> mockedOwnerRepo;
        private Mock<IMechanicRepository> mockedMechanicRepo;
        private Mock<IOrderRepository> mockedOrderRepo;
        private Mock<IVehicleRepository> mockedVehicleRepo;
        private Mock<IRepairRepository> mockedRepairRepo;

        private CarServiceLogic logic;

        /// <summary>
        /// Create mocked repositories.
        /// Fill lists with dummy data.
        /// Set mocked repositories to return dummy data.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedOwnerRepo = new Mock<IOwnerRepository>();
            this.mockedMechanicRepo = new Mock<IMechanicRepository>();
            this.mockedOrderRepo = new Mock<IOrderRepository>();
            this.mockedVehicleRepo = new Mock<IVehicleRepository>();
            this.mockedRepairRepo = new Mock<IRepairRepository>();

            List<Tulajdonos> ownerDummyData = new List<Tulajdonos>()
            {
                new Tulajdonos() { Nev = "tulajdonos név", Telefonszam = 1111, Cím = "tulajdonos cím", Email_cim = "tulajdonos email cím" }
            };

            List<Szerelo> mechanicDummyData = new List<Szerelo>()
            {
                new Szerelo() { Szerelo_ID = 1, Nev = "szerelő név", Kepzettseg = "mester", Muhely_cime = "műhely címe" }
            };

            List<Megrendeles> orderDummyData = new List<Megrendeles>()
            {
                new Megrendeles() { Megrendeles_datum = new DateTime(2019, 1, 10), Megrendeles_ID = 1, Szerelo_ID = 1, Nev = "tulajdonos név", Telefonszam = 1111 }
            };

            List<Gepjarmu> vehicleDummyData = new List<Gepjarmu>()
            {
                new Gepjarmu() { Modell = "ford", Tipus = "thunderbird", Alvazszam = "alvazszam", Gyartasi_ev = 1994, Szin = "ezüst", Kivitel = "coupe", Nev = "tulajdonos név", Telefonszam = 1111, Szerelo_ID = 1 }
            };

            List<Szereles> repairDummyData = new List<Szereles>()
            {
                new Szereles() { Alvazszam = "alvazszam", Datum = new DateTime(2019, 1, 10), Szerelo_ID = 1, Elvegzett_munka = "vízpumpa csere" }
            };

            this.mockedOwnerRepo.Setup(mo => mo.ReadAll()).Returns(ownerDummyData.AsQueryable());
            this.mockedMechanicRepo.Setup(mm => mm.ReadAll()).Returns(mechanicDummyData.AsQueryable());
            this.mockedOrderRepo.Setup(mo => mo.ReadAll()).Returns(orderDummyData.AsQueryable());
            this.mockedVehicleRepo.Setup(mv => mv.ReadAll()).Returns(vehicleDummyData.AsQueryable());
            this.mockedRepairRepo.Setup(mr => mr.ReadAll()).Returns(repairDummyData.AsQueryable());

            this.logic = new CarServiceLogic(this.mockedMechanicRepo.Object, this.mockedOrderRepo.Object, this.mockedOwnerRepo.Object, this.mockedRepairRepo.Object, this.mockedVehicleRepo.Object);
        }

        /// <summary>
        /// Check that logic returns proper table names.
        /// </summary>
        [Test]
        public void CheckIfReturnedTableNamesCorrect()
        {
            string[] resultTableNames = this.logic.GetTableNames();

            string[] expectedTableNames = { "mechanic", "order", "owner", "repair", "vehicle" };

            Assert.That(resultTableNames, Is.EqualTo(expectedTableNames));
        }

        /// <summary>
        /// Check if logic return the proper column names of the given table.
        /// </summary>
        /// <param name="tableName">Table name to get its columns.</param>
        [TestCase("mechanic")]
        [TestCase("order")]
        [TestCase("owner")]
        [TestCase("repair")]
        [TestCase("vehicle")]
        public void CheckIfReturnedTableColumnsCorrect(string tableName)
        {
            // Arrange
            Dictionary<string, string[]> columnNamesByTables;

            string[] mechanicColumns = { "mechanic ID", "mechanic's name", "qualification", "workshop address" };
            string[] orderColumns = { "order ID", "order date", "mechanic ID", "owner's name", "owner's telephone" };
            string[] ownerColumns = { "name", "telephone", "address", "e-mail" };
            string[] repairColumns = { "work done", "work date", "vehicle identification number", "mechanic ID" };
            string[] vehicleColumns = { "vehicle identification number", "manufacturer", "model", "body style", "color", "production date", "owner's name", "owner's telephone", "mechanic ID" };

            columnNamesByTables = new Dictionary<string, string[]>();

            columnNamesByTables.Add("mechanic", mechanicColumns);
            columnNamesByTables.Add("order", orderColumns);
            columnNamesByTables.Add("owner", ownerColumns);
            columnNamesByTables.Add("repair", repairColumns);
            columnNamesByTables.Add("vehicle", vehicleColumns);

            string[] columnNamesExpected = columnNamesByTables[tableName];

            // Act
            string[] columnNamesResult = this.logic.GetTableColumns(tableName);

            // Assert
            Assert.That(columnNamesResult, Is.EqualTo(columnNamesExpected));
        }

        /// <summary>
        /// Checks if a database element was created.
        /// </summary>
        [Test]
        public void CheckIfCreateMethodIsCalled()
        {
            string[] dataToCreateTestOwner = { "owner", "Test Name", "1234", "Test Address", "Test Email" };
            this.logic.Create(dataToCreateTestOwner);
            this.mockedOwnerRepo.Verify(x => x.Create(It.IsAny<Tulajdonos>()), Times.Once);
        }

        /// <summary>
        /// Checks if a database element was updated.
        /// </summary>
        [Test]
        public void CheckIfUpdateMethodIsCalled()
        {
            string[] dataToTestUpdate = { "mechanic", "1", "mechanic's name", "Test" };
            this.logic.Update(dataToTestUpdate);
            this.mockedMechanicRepo.Verify(x => x.Update(It.IsAny<string[]>()), Times.Once);
        }

        /// <summary>
        /// Checks if a database element was deleted through that it verifies the getbyid call.
        /// </summary>
        [Test]
        public void CheckIfDeleteMethodIsCalled()
        {
            string[] dataToTestDelete = { "mechanic", "1", " " };
            this.logic.Delete(dataToTestDelete);

            this.mockedMechanicRepo.Verify(x => x.GetById(It.IsAny<string[]>()), Times.Once);
        }

        /// <summary>
        /// Check if an exception is not thrown while create.
        /// (It is simply not created.)
        /// </summary>
        [Test]
        public void NotThrowOnWrongDataCreate()
        {
            string[] testData = { "wrongtable", "wrongdata" };

            Assert.That(() => this.logic.Create(testData), Throws.Nothing);
        }

        /// <summary>
        /// Readall method gives the right string if not matched table.
        /// </summary>
        [Test]
        public void ReadallGivesMessageOnWrongTableName()
        {
            Assert.That(this.logic.ReadAll("wrongTableName"), Is.EqualTo("Not valid table name."));
        }

        [TestCase("mechanic")]
        [TestCase("order")]
        [TestCase("owner")]
        [TestCase("repair")]
        [TestCase("vehicle")]
        public void ReadallReturnTypeIsAString(string tableName)
        {
            var result = this.logic.ReadAll(tableName);

            Assert.That(result.GetType(), Is.EqualTo(typeof(string)));
        }

        /// <summary>
        /// Checks if we call the method with valid mechanicID, that it will return something and not an empty string.
        /// GetMechanicOfRepair - test
        /// </summary>
        /// <param name="mechanicId">Id of mechanic to get.</param>
        [TestCase(1)]
        public void CheckIfReturnMechanicOnValidId(int mechanicId)
        {
            var result = this.logic.GetMechanicOfRepair(mechanicId);

            Assert.That(result, Is.Not.Null);
        }

        /// <summary>
        /// Checks if we call the method with wrong mechanicID, that it will return an empty string.
        /// GetMechanicOfRepair - test
        /// </summary>
        /// <param name="mechanicId">Id of mechanic to get.</param>
        [TestCase(900)]
        public void CheckIfReturnEmptyOnWrongId(int mechanicId)
        {
            var result = this.logic.GetMechanicOfRepair(mechanicId);

            Assert.That(result, Is.EqualTo(null));
        }

        /// <summary>
        /// Check if the method gets a valid id, will return an owner.
        /// GetVehicleOwner - test
        /// </summary>
        /// <param name="vin">vehicle id</param>
        [TestCase("alvazszam")]
        public void CheckIfReturnOwnerOnValidId(string vin)
        {
            var result = this.logic.GetVehicleOwner(vin);

            Assert.That(result, Is.Not.Null);
        }

        /// <summary>
        /// Check if the method gets not valid id, will return an empty string.
        /// GetVehicleOwner - test
        /// </summary>
        /// <param name="vin">vehicle id</param>
        [TestCase("wrong input")]
        public void CheckIfReturnEmptyOnNotValidId(string vin)
        {
            var result = this.logic.GetVehicleOwner(vin);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Checks that it wont return null as the table is not empty.
        /// GroupMechanicsByQualification - test
        /// </summary>
        [Test]
        public void CheckIfReturnNotNullOnNoNEmptyTable()
        {
            var result = this.logic.GroupMechanicsByQualification();

            Assert.That(result, Is.Not.Null);
        }

        /// <summary>
        /// Checks that the return type is a string.
        /// GroupMechanicsByQualification - test
        /// </summary>
        [Test]
        public void CheckIfReturnTypeIsString()
        {
            var result = this.logic.GroupMechanicsByQualification();

            Assert.That(result.GetType(), Is.EqualTo(typeof(string)));
        }
    }
}
