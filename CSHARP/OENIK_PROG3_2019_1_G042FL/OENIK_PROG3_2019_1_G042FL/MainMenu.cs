﻿// <copyright file="MainMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_1_G042FL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Logic;

    /// <summary>
    /// Class for Main Menu methods.
    /// Keep contact with user.
    /// </summary>
    public class MainMenu : IMainMenu
    {
        private readonly CarServiceLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="logic">To initialize a new instance of logic.</param>
        public MainMenu(CarServiceLogic logic)
        {
            this.logic = logic;
        }

        public void RunMainMenu()
        {
            bool exit = false;
            string selectedTableForCrud = string.Empty;

            while (!exit)
            {
                // Cases from 0 to getLogicFunctions.Length - 1
                switch (this.SelectFromMainMenuElements(this.logic.GetLogicFunctions()))
                {
                    // Create
                    case 0:
                        selectedTableForCrud = this.TableSelectorForCRUD(this.logic.GetTableNames(), this.logic.GetLogicFunctions()[0]);
                        if (selectedTableForCrud == "exit")
                        {
                            break;
                        }

                        this.logic.Create(this.GetDataForCreate(selectedTableForCrud));

                        this.PressKeyWaitKey();
                        break;

                    // ReadAll
                    case 1:
                        selectedTableForCrud = this.TableSelectorForCRUD(this.logic.GetTableNames(), this.logic.GetLogicFunctions()[1]);
                        if (selectedTableForCrud == "exit")
                        {
                            break;
                        }

                        Console.WriteLine(this.logic.ReadAll(selectedTableForCrud));

                        this.PressKeyWaitKey();
                        break;

                    // Update
                    case 2:
                        selectedTableForCrud = this.TableSelectorForCRUD(this.logic.GetTableNames(), this.logic.GetLogicFunctions()[2]);
                        if (selectedTableForCrud == "exit")
                        {
                            break;
                        }

                        // primary and foreign keys cannot be updated.
                        this.logic.Update(this.GetDataForUpdate(selectedTableForCrud));

                        this.PressKeyWaitKey();
                        break;

                    // Delete
                    case 3:
                        selectedTableForCrud = this.TableSelectorForCRUD(this.logic.GetTableNames(), this.logic.GetLogicFunctions()[3]);
                        if (selectedTableForCrud == "exit")
                        {
                            break;
                        }

                        Console.WriteLine("The table data can be deleted in a following order:\nMegrendeles, Szereles, Gepjarmu, Szerelo, Tulajdonos");

                        this.logic.Delete(this.GetDataForDelete(selectedTableForCrud));
                        this.PressKeyWaitKey();
                        break;

                    // Find the mechanic had done the repair
                    case 4:
                        Console.Clear();
                        Console.WriteLine(this.logic.GetMechanicOfRepair(this.GetDataForGetMechanicRepairCall()));

                        this.PressKeyWaitKey();
                        break;

                    case 5:
                        Console.Clear();
                        Console.WriteLine(this.logic.GetVehicleOwner(this.GetDataForGetVehicleOwnerCall()));

                        // e.g.: VIN: 3mgldf67lf3
                        this.PressKeyWaitKey();
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine(this.logic.GroupMechanicsByQualification());

                        this.PressKeyWaitKey();
                        break;
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Special last minute offer:");
                        try
                        {
                            Console.WriteLine(this.logic.JavaEndpointQuery(this.GetDataForJavaCall()));
                        }
                        catch (WebException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        this.PressKeyWaitKey();
                        break;
                    case 8:
                        Console.WriteLine("Exiting...");
                        exit = true;
                        this.PressKeyWaitKey();
                        break;
                    default:
                        Console.WriteLine("Ide lehetetlen eljutni, de miért ne maradhatna itt?!");
                        exit = true;
                        this.PressKeyWaitKey();
                        break;
                }
            }
        }

        /// <summary>
        /// Collects data from console to give to GetVehicleOwner method parameter.
        /// </summary>
        /// <returns>Returns the choosen string.</returns>
        private string GetDataForGetVehicleOwnerCall()
        {
            Console.WriteLine("Please type in a valid Vehicle Identification Number to get its owner!");

            string temp;
            string choosen;
            this.TryRead(Console.ReadLine(), out temp);
            choosen = temp;

            return choosen;
        }

        /// <summary>
        /// Collects data from console to give to GetMechanicRepair method parameter.
        /// </summary>
        /// <returns>Returns the choosen int.</returns>
        private int GetDataForGetMechanicRepairCall()
        {
            Console.WriteLine("Please type in a valid mechanic_ID to get its repair!");

            string temp;
            int choosen;
            this.TryRead(Console.ReadLine(), out temp);
            choosen = int.Parse(temp);

            return choosen;
        }

        /// <summary>
        /// Collects data from console to give to Java.
        /// </summary>
        /// <returns>Returns the choosen int (0, 1, 2)</returns>
        private int GetDataForJavaCall()
        {
            Console.WriteLine("Please type in 0, 1 or 2 to get your last minute costum price offer!");

            string temp;
            int choosen;
            this.TryRead(Console.ReadLine(), out temp);
            choosen = int.Parse(temp);

            while (!(choosen <= 0 && choosen <= 2))
            {
                this.TryRead(Console.ReadLine(), out temp);
                choosen = int.Parse(temp);
            }

            return choosen;
        }

        /// <summary>
        /// Collects data from console to delete a database element.
        /// </summary>
        /// <param name="tableName">Table name to delete in.</param>
        /// <returns>Data required to delete a database element.</returns>
        private string[] GetDataForDelete(string tableName)
        {
            // The value on zero index is the table name. The rest are key(s). (Max. 2 in this DB.)
            string[] dataToGiveLogicDelete = new string[3];
            dataToGiveLogicDelete[0] = tableName;

            string input = string.Empty;

            // Menu instructon - primary key.
            Console.WriteLine("Please type in the primary key of field need to deleted from " + tableName + " :");

            // Validate console input
            while (!this.TryRead(Console.ReadLine(), out input))
            {
                Console.WriteLine("Please type in text!");
            }

            dataToGiveLogicDelete[1] = input;

            // Menu instruction second primary key if it exists.
            Console.WriteLine("If the table has a second primary key, please give it as well.");
            Console.WriteLine("If don't then hit enter.");
            this.TryRead(Console.ReadLine(), out input);
            dataToGiveLogicDelete[2] = input;

            return dataToGiveLogicDelete;
        }

        /// <summary>
        /// Collects data from console to update a database element.
        /// </summary>
        /// <param name="tableName">Table name to update in.</param>
        /// <returns>Data required to update.
        /// Returned strings are: tablename, primarykey(s), data</returns>
        private string[] GetDataForUpdate(string tableName)
        {
            string[] updateableColumns = this.logic.GetUpdatableColumnNames(tableName);

            // The value on zero index is the table name. It must be given to logic.
            string[] dataToGiveLogicUpdate = new string[5];
            dataToGiveLogicUpdate[0] = tableName;

            Console.WriteLine("Which column do you want to update?");
            int choosen = this.SelectFromMainMenuElements(updateableColumns);

            if (tableName == "owner")
            {
                Console.WriteLine("Please type in the elements primary key 'name' you want to update");
                string choosenPrimaryKey;
                this.TryRead(Console.ReadLine(), out choosenPrimaryKey);
                dataToGiveLogicUpdate[1] = choosenPrimaryKey;

                Console.WriteLine("Please type in the elements primary key 'telephone' you want to update");
                string choosenPrimaryKey2;
                this.TryRead(Console.ReadLine(), out choosenPrimaryKey2);
                dataToGiveLogicUpdate[2] = choosenPrimaryKey2;

                dataToGiveLogicUpdate[3] = updateableColumns[choosen];

                Console.WriteLine("Please enter the new data:");
                string newData;
                this.TryRead(Console.ReadLine(), out newData);
                dataToGiveLogicUpdate[4] = newData;
            }
            else
            {
                Console.WriteLine("Please type in the elements primary key you want to update");
                string choosenPrimaryKey;
                this.TryRead(Console.ReadLine(), out choosenPrimaryKey);
                dataToGiveLogicUpdate[1] = choosenPrimaryKey;

                dataToGiveLogicUpdate[2] = updateableColumns[choosen];

                Console.WriteLine("Please enter the new data:");
                string newData;
                this.TryRead(Console.ReadLine(), out newData);
                dataToGiveLogicUpdate[3] = newData;
            }

            return dataToGiveLogicUpdate;
        }

        /// <summary>
        /// Collects data from console for the new database element in a correct form.
        /// </summary>
        /// <param name="tableName">Table name to create in.</param>
        /// <returns>Data required to make new database element.</returns>
        private string[] GetDataForCreate(string tableName)
        {
            string[] tableColumns = this.logic.GetTableColumns(tableName);

            // The value on zero index is the table name. It must be given to logic.
            string[] dataToGiveLogicCreate = new string[tableColumns.Length + 1];
            dataToGiveLogicCreate[0] = tableName;

            for (int i = 0; i < tableColumns.Length; i++)
            {
                string input = string.Empty;

                // Menu instructon
                Console.WriteLine("Please type in the new " + tableName + "'s " + tableColumns[i] + " :");

                // Validate console input
                while (!this.TryRead(Console.ReadLine(), out input))
                {
                    Console.WriteLine("Please type in text!");
                }

                dataToGiveLogicCreate[i + 1] = input;
            }

            return dataToGiveLogicCreate;
        }

        /// <summary>
        /// Method to call when any operation is completed and the console need to be refeshed.
        /// </summary>
        private void PressKeyWaitKey()
        {
            Console.WriteLine("Press ENTER to proceed!");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// Writes out main menu elements to console.
        /// Reads data from console to get the selection.
        /// Validate input.
        /// </summary>
        /// <param name="functionsToSelectFrom">Choosable menu elements.</param>
        /// <returns>Valid main menu element selected.</returns>
        private int SelectFromMainMenuElements(string[] functionsToSelectFrom)
        {
            int input;
            string mainMenuInstruction = "Please write the choosen menu element's number then press enter";

            this.WriteMenuElements(functionsToSelectFrom, mainMenuInstruction);
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                this.WriteMenuElements(functionsToSelectFrom, mainMenuInstruction);
                Console.WriteLine("Please enter a valid numerical value!");
            }

            return input;
        }

        /// <summary>
        /// Writes out main menu elements to console.
        /// Reads data from console to get the selection.
        /// Validate input.
        /// </summary>
        /// <param name="validTableNames">Table names to select from.</param>
        /// <param name="selectedCRUD">The crud method's name would be done on the selected table.</param>
        /// <returns>Valid table name selected.</returns>
        private string TableSelectorForCRUD(string[] validTableNames, string selectedCRUD)
        {
            int input;
            string selectedTable = string.Empty;
            string crudTableMenuInstruction = "Please write the choosen menu element's number then press enter \nto select table to ";
            crudTableMenuInstruction += selectedCRUD;
            crudTableMenuInstruction += " data in it!\n";
            crudTableMenuInstruction += "(Phone numbers and ID ending fields cannot start with 0\n";
            crudTableMenuInstruction += "  and cannot be more than 9 characters long! - These fields must be numeric!\n";
            crudTableMenuInstruction += "  Date like fields must be written in a following form: YYYY.MM.DD !\n";
            crudTableMenuInstruction += "  Foreign keys must be matched! Use read first...) \n";

            this.WriteMenuElements(validTableNames, crudTableMenuInstruction);

            // Validate input
            while ((!int.TryParse(Console.ReadLine(), out input)) && (input >= 0) && (input <= validTableNames.Length))
            {
                this.WriteMenuElements(validTableNames, crudTableMenuInstruction);
                Console.WriteLine("Please enter a valid numerical value!");
            }

            // Input numbers can be from 0 to validTableNames.Length,
            // where the last number is not an index but the exit option.
            if (input == validTableNames.Length)
            {
                selectedTable = "exit";
            }
            else
            {
                selectedTable = validTableNames[input];
            }

            return selectedTable;
        }

        /// <summary>
        /// Like the TryParse method for integer.
        /// Check if the input string is null or empty.
        /// </summary>
        /// <param name="readLine">Raw data from console.</param>
        /// <param name="input">arameter string to pass back read line value if it is valid.</param>
        /// <returns>False if null or empty string was given. True otherwise</returns>
        private bool TryRead(string readLine, out string input)
        {
            if (!string.IsNullOrEmpty(readLine))
            {
                input = readLine;
                return true;
            }

            input = string.Empty;

            return false;
        }

        /// <summary>
        /// Write menu elements to console in a format.
        /// </summary>
        /// <param name="elements">Collection of strings to write.</param>
        /// <param name="menuInstruction">Instruction for the menu. Write first.</param>
        private void WriteMenuElements(string[] elements, string menuInstruction)
        {
            int i;

            Console.Clear();
            Console.WriteLine(menuInstruction);

            for (i = 0; i < elements.Length; i++)
            {
                Console.WriteLine(i + ". " + elements[i]);
            }

            Console.WriteLine(i + ". " + "exit");
        }
    }
}
