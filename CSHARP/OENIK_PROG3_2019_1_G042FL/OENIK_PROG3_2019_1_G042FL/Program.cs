﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_1_G042FL
{
    using System;
    using CarService.Logic;

    public class Program
    {
        public static void Main(string[] args)
        {
            CarServiceLogic logic = new CarServiceLogic();
            MainMenu menu = new MainMenu(logic);

            Console.SetWindowSize(90, 40);

            menu.RunMainMenu();
        }
    }
}
