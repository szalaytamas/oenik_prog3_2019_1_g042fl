﻿// <copyright file="IMainMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_1_G042FL
{
    /// <summary>
    /// Interface for MainMenu methods.
    /// </summary>
    public interface IMainMenu
    {
        /// <summary>
        /// The main menu root method.
        /// </summary>
        void RunMainMenu();
    }
}
