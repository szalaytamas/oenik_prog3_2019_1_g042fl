//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Megrendeles
    {
        public decimal Megrendeles_ID { get; set; }
        public Nullable<System.DateTime> Megrendeles_datum { get; set; }
        public Nullable<decimal> Szerelo_ID { get; set; }
        public string Nev { get; set; }
        public Nullable<decimal> Telefonszam { get; set; }
    
        public virtual Tulajdonos Tulajdonos { get; set; }
        public virtual Szerelo Szerelo { get; set; }
    }
}
