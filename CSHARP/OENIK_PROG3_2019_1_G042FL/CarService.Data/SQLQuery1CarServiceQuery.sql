﻿DROP TABLE Megrendeles;
DROP TABLE Szereles;
DROP TABLE Gepjarmu;
DROP TABLE Szerelo;
DROP TABLE Tulajdonos;




CREATE TABLE Tulajdonos(
Nev varchar(50),
Telefonszam numeric(11),
Cím varchar(50),
Email_cim varchar(50),
CONSTRAINT nev_tel_pk PRIMARY KEY(Nev, Telefonszam)
);


CREATE TABLE Szerelo(
Nev varchar(50),
Kepzettseg varchar(30),
Muhely_cime varchar(50),
Szerelo_ID numeric(4),
CONSTRAINT szerelo_ID_pk PRIMARY KEY(Szerelo_ID)
);

CREATE TABLE Gepjarmu(
Modell varchar(20),
Tipus varchar(10),
Gyartasi_ev numeric(4),
Alvazszam varchar(40),
Kivitel varchar(20),
Szin varchar(10),
Szerelo_ID numeric(4),
Nev varchar(50),
Telefonszam numeric(11),
CONSTRAINT alvazszam_pk PRIMARY KEY(Alvazszam),
CONSTRAINT szerelo_ID_gj_fk FOREIGN KEY(Szerelo_ID) REFERENCES Szerelo(Szerelo_ID),
CONSTRAINT nev_tel_gj_fk FOREIGN KEY(Nev, Telefonszam) REFERENCES Tulajdonos(Nev, Telefonszam)
);



CREATE TABLE Szereles(
Elvegzett_munka varchar(50),
Datum date,
Alvazszam varchar(40),
Szerelo_ID numeric(4),
CONSTRAINT munka_datum_pk PRIMARY KEY(Elvegzett_munka, datum),
CONSTRAINT szerelo_ID_sz_fk FOREIGN KEY(Szerelo_ID) REFERENCES Szerelo(Szerelo_ID),
CONSTRAINT alvazszam_fk FOREIGN KEY(Alvazszam) REFERENCES Gepjarmu(Alvazszam)
);

CREATE TABLE Megrendeles(
Megrendeles_ID numeric(4),
Megrendeles_datum date,
Szerelo_ID numeric(4),
Nev varchar(50),
Telefonszam numeric(11)
CONSTRAINT megrendeles_ID_pk PRIMARY KEY(Megrendeles_ID)
CONSTRAINT szerelo_ID_m_fk FOREIGN KEY(Szerelo_ID) REFERENCES Szerelo(Szerelo_ID),
CONSTRAINT nev_tel_m_fk FOREIGN KEY(Nev, Telefonszam) REFERENCES Tulajdonos(Nev, Telefonszam)
);






INSERT INTO Szerelo 
VALUES('Oldd Józsi', 'autószerelő', '1048 Budapest, Dózsa György út 15', 0001); 
INSERT INTO Szerelo 
VALUES('Meg Józsi', 'autószerelő', '1048 Budapest, Dózsa György út 53', 0002);
INSERT INTO Szerelo 
VALUES('Okosba Józsi','autószerelő', '1135 Budapest, Jász utca 46', 0003); 
INSERT INTO Szerelo 
VALUES('Péntek Elek','autószerelő', '1115 Budapest, Autószerelők útja 46', 0004); 
INSERT INTO Szerelo 
VALUES('Cserepes Virág','autószerelő', '1118 Budapest, Rétköz utca 16', 0005); 
INSERT INTO Szerelo 
VALUES('Végh Béla', 'gyakornok', '1045 Budapest, Rózsa utca utca 1', 0006);
INSERT INTO Szerelo 
VALUES('Vanbaj Vendel', 'gyakornok', '2000 Szentendre, Vasúti villa sor 47', 0007);
INSERT INTO Szerelo 
VALUES('Papp László', 'autószerelő mester', '1172 Budapest, Rétihéja utca 17', 0008);
INSERT INTO Szerelo 
VALUES('Nikola Tesla', 'autószerelő mester', '2120 Dunakeszi Kossuth Lajos utca 42', 0009);
INSERT INTO Szerelo 
VALUES('Kovács György', 'autószerelő mester', '1042 Budapest, Laborfalvi Róza utca 18', 0010);



INSERT INTO Tulajdonos 
VALUES('Szalay Tamás',06706231177 , '1048 Budapest, Szíjgyártó utca 4', 'tomi.szalay456@gmail.com'); 
INSERT INTO Tulajdonos 
VALUES('Majer Tünde',06306431517 , '1222 Budapest Brassói utca 10', 'majer.tunde@gmail.com'); 
INSERT INTO Tulajdonos 
VALUES('Kovács Máté',06705251155 , '1048 Budapest, Bőröndös utca 64', 'kovacs.mate@gmail.com');
INSERT INTO Tulajdonos 
VALUES('Litavszky Gergely',06806299197 , '2120 Dunakeszi, Kőzúzó utca 31', 'lito.geri@gmail.com');
INSERT INTO Tulajdonos 
VALUES('Pongó Gábor',06306991177 , '2000 Szentendre, Energiaital utca 9000', 'pongo.gabor@gmail.com'); 
INSERT INTO Tulajdonos 
VALUES('Pénzes Bea',06306991188 , '2000 Szentendre, Energiaital utca 9000', 'penzes.bea@gmail.com'); 
INSERT INTO Tulajdonos 
VALUES('Illés Mónika',06704571227 , '2100 Gödöllő, Szilágyi Erzsébet fasor 1', 'illes.monika@gmail.com'); 
INSERT INTO Tulajdonos 
VALUES('Németh Márk',06309971557 , '1043 Budapest, Szigeti József utca 7', '@gmail.com');
INSERT INTO Tulajdonos 
VALUES('Hoffman András', 06202908834 , '3292 Adács Erzsébet tér 26', 'hoffman.andras@gmail.com');
INSERT INTO Tulajdonos 
VALUES('Thury Aurelia', 06709631227 ,'2694 Magyarnándor Hegedűs Gyula utca 71', 'thury.aurelia@gmail.com');
INSERT INTO Tulajdonos 
VALUES('Nagy Róbert',06304541288 ,'2381 Tarján, Erzsébet körút 19', 'nagy.robert@gmail.com');




INSERT INTO Gepjarmu
VALUES('Mercedes-Benz', 'W201', 1992, 'mbw190ekejet123','sedan', 'ezüst', 0001 ,'Szalay Tamás', 06706231177);
INSERT INTO Gepjarmu
VALUES('Toyota', 'Tundra', 2010, 'tt20crf34di4t', 'pickup', 'fekete', 0002, 'Majer Tünde', 06306431517);
INSERT INTO Gepjarmu
VALUES('Fiat', 'Barchetta', 2001, '54tj34n5345', 'cabrio', 'piros', 0003, 'Kovács Máté', 06705251155);
INSERT INTO Gepjarmu
VALUES('Honda', 'Civic', 2018, '3mgldf67lf3', 'ferdehátú', 'fekete', 0004, 'Litavszky Gergely', 06806299197);
INSERT INTO Gepjarmu
VALUES('Volvo', 'S60', 2006, 'df9fg3gj39hookk', 'sedan', 'fehér', 0005, 'Pongó Gábor', 06306991177 );
INSERT INTO Gepjarmu
VALUES('Volvo', 'S40', 2006, 'vos435tdae07', 'sedan', 'fehér', 0005, 'Pénzes Bea', 06306991188 );
INSERT INTO Gepjarmu
VALUES('Hyundai', 'Terracan', 2004, 'hyt40cr29dhp2', 'zárt terepjáró', 'fekete', 0006, 'Illés Mónika', 06704571227);
INSERT INTO Gepjarmu
VALUES('Renault', 'Twingo', 1997, 'regg355gefgeek', 'ferdehátú', 'kék', 0007, 'Hoffman András', 06202908834);
INSERT INTO Gepjarmu
VALUES('Opel', 'Omega', 2002, 'oo344plk45kfv', 'sedan', 'szürke', 0008, 'Thury Aurelia', 06709631227);
INSERT INTO Gepjarmu
VALUES('Opel', 'Astra', 2000, 'oa21fmdgj345cdi', 'sedan', 'fekete', 0009, 'Németh Márk', 06309971557 );
INSERT INTO Gepjarmu
VALUES('Mercedes-Benz', 'c220', 2002, 'mbw220cdi22ced', 'coupe', 'ezüst', 0010, 'Nagy Róbert', 06304541288);




INSERT INTO Szereles
VALUES('Hengerfej gépmunka', '2018.02.01', 'mbw190ekejet123',0001);
INSERT INTO Szereles
VALUES('Olajak szűrők', ' 2018.03.01', 'tt20crf34di4t', 0002);
INSERT INTO Szereles
VALUES('Vezérlés csere', ' 2018.04.01', '54tj34n5345', 0003);
INSERT INTO Szereles
VALUES('jobb kormányöszekötő', ' 2018.05.01', '3mgldf67lf3', 0004);
INSERT INTO Szereles
VALUES('turbó felújítás', ' 2018.06.01', 'df9fg3gj39hookk', 0005);
INSERT INTO Szereles
VALUES('magasnyomású pumpa csere', ' 2018.07.01', 'hyt40cr29dhp2', 0006);
INSERT INTO Szereles
VALUES('kerékagyacsapágy csere', ' 2018.08.01', 'regg355gefgeek', 0007);
INSERT INTO Szereles
VALUES('kettőstömegű lendkerék csere', ' 2018.09.01', 'oo344plk45kfv', 0008);
INSERT INTO Szereles
VALUES('féktárcsák csere', ' 2018.10.01', 'oa21fmdgj345cdi', 0009);
INSERT INTO Szereles
VALUES('fagyálló beállítás', ' 2018.11.01', 'mbw220cdi22ced', 0010);

/*

Megrendeles_ID numeric(4),
Megrendeles_datum date,
Szerelo_ID numeric(4),
Nev varchar(50),
Telefonszam numeric(11)

*/


INSERT INTO Megrendeles
VALUES(0001, '2018.01.01', 0001,'Szalay Tamás', 06706231177);
INSERT INTO Megrendeles
VALUES(0002, '2018.02.01', 0002, 'Majer Tünde', 06306431517);
INSERT INTO Megrendeles
VALUES(0003, '2018.03.01', 0003, 'Kovács Máté', 06705251155);
INSERT INTO Megrendeles
VALUES(0004, '2018.04.01', 0004, 'Litavszky Gergely', 06806299197);
INSERT INTO Megrendeles
VALUES(0005, '2018.05.01', 0005, 'Pongó Gábor', 06306991177);
INSERT INTO Megrendeles
VALUES(0006, '2018.05.01', 0006, 'Pénzes Bea', 06306991188);
INSERT INTO Megrendeles
VALUES(0007, '2018.06.01', 0007, 'Illés Mónika', 06704571227);
INSERT INTO Megrendeles
VALUES(0008, '2018.07.01', 0008, 'Hoffman András', 06202908834);
INSERT INTO Megrendeles
VALUES(0009, '2018.08.01', 0009, 'Thury Aurelia', 06709631227);
INSERT INTO Megrendeles
VALUES(0010, '2018.09.01', 0010, 'Németh Márk', 06309971557);
INSERT INTO Megrendeles
VALUES(0011, '2018.10.01', 0001, 'Nagy Róbert', 06304541288);
