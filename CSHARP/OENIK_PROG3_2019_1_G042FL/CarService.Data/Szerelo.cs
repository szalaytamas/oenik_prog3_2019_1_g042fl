//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Szerelo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Szerelo()
        {
            this.Gepjarmu = new HashSet<Gepjarmu>();
            this.Megrendeles = new HashSet<Megrendeles>();
            this.Szereles = new HashSet<Szereles>();
        }
    
        public string Nev { get; set; }
        public string Kepzettseg { get; set; }
        public string Muhely_cime { get; set; }
        public decimal Szerelo_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Gepjarmu> Gepjarmu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Megrendeles> Megrendeles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Szereles> Szereles { get; set; }
    }
}
