﻿// <copyright file="IJava.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.JavaWeb
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for Java endpoint methods.
    /// </summary>
    public interface IJava
    {
        /// <summary>
        /// Get data from java endpoint and process it.
        /// </summary>
        /// <param name="parameterPassedToJava">Data to java servlet. Can be 0, 1, 2.</param>
        /// <returns>Returns the response of java endpoint.</returns>
        string GetDataJavaEndpoint(int parameterPassedToJava);
    }
}
