﻿// <copyright file="Java.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.JavaWeb
{
    using System.Xml.Linq;

    /// <summary>
    /// Class for getting data from java and process it.
    /// </summary>
    public class Java : IJava
    {
        /// <summary>
        /// Get data from java endpoint.
        /// </summary>
        /// <param name="parameterPassedToJava">Data to java servlet. Can be 0, 1, 2.</param>
        /// <returns>Returns the response of java endpoint.</returns>
        public string GetDataJavaEndpoint(int parameterPassedToJava)
        {
            string output = string.Empty;

            if (parameterPassedToJava >= 0 && parameterPassedToJava <= 2)
            {
                string url = "http://localhost:8080/JavaMenu/javacalling?myVariable=";

                url += parameterPassedToJava;

                XDocument readJavaPage = XDocument.Load(url);

                // var result = readJavaPage.Element("autos").Elements();
                output = string.Format(
                    $"Car: {readJavaPage.Element("autos").Element("names").Value}\n Dealer: {readJavaPage.Element("autos").Element("dealers").Value}\n Price: {readJavaPage.Element("autos").Element("prices").Value}\n");
            }
            else
            {
                output = "Not valid input for JavaEndpoint";
            }

            return output;
        }
    }
}
