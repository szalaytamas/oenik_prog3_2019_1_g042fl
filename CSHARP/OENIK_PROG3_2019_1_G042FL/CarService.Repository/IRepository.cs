﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface to specify CRUD methods for CarService Database
    /// </summary>
    /// <typeparam name="T">Type of class the methods get</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Create a new CarService database element defined by generic parameter
        /// </summary>
        /// <param name="newData">T is a class to create</param>
        void Create(T newData);

        /// <summary>
        /// Reads all data from table
        /// </summary>
        /// <returns>A queryable collection of database elements</returns>
        IQueryable<T> ReadAll();

        /// <summary>
        /// Deletes a Carservice database element defined by generic parameter.
        /// </summary>
        /// <param name="dataToDelete">Generic parameter to specify data to delete.</param>
        void Delete(T dataToDelete);

        /// <summary>
        /// Find the database element by its id
        /// </summary>
        /// <param name="id">Id to find</param>
        /// <returns>Found database element, or null if there is nothing identified by id</returns>
        T GetById(string[] id);
    }
}
