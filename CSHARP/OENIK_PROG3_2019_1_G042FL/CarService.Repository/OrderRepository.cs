﻿// <copyright file="OrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System.Linq;
    using CarService.Data;

    public class OrderRepository : IOrderRepository
    {
        private CarServiceDatabaseEntities carServiceDataEntities;

        public OrderRepository(CarServiceDatabaseEntities carServiceDataEntities)
        {
            this.carServiceDataEntities = carServiceDataEntities;
        }

        /// <summary>
        /// Creates a new field in order table.
        /// </summary>
        /// <param name="newData">Data to fill the new field.</param>
        public void Create(Megrendeles newData)
        {
            this.carServiceDataEntities.Megrendeles.Add(newData);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Delete a specified order field of the table.
        /// </summary>
        /// <param name="dataToDelete">Specified object to delete from database</param>
        public void Delete(Megrendeles dataToDelete)
        {
            this.carServiceDataEntities.Megrendeles.Remove(dataToDelete);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Find the database element by its id
        /// </summary>
        /// <param name="id">Id to find. id[0] Megrendeles_ID</param>
        /// <returns>Found database element, or null if there is nothing identified by id</returns>
        public Megrendeles GetById(string[] id)
        {
            if (this.carServiceDataEntities.Megrendeles.Find(int.Parse(id[0])) != null)
            {
                return this.carServiceDataEntities.Megrendeles.Find(int.Parse(id[0]));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all data from the Order table as a queryable collection.
        /// </summary>
        /// <returns>Queryable collection.</returns>
        public IQueryable<Megrendeles> ReadAll()
        {
            return this.carServiceDataEntities.Megrendeles;
        }
    }
}
