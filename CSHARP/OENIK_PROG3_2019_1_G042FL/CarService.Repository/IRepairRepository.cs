﻿// <copyright file="IRepairRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using CarService.Data;

    /// <summary>
    /// Interface for repair specific methods
    /// JobDone and Date are primary keys
    /// VIN and MechanicID are foreign keys
    /// It doesn't have methods, because it only contains primary and foreign keys and they cannot be updated.
    /// </summary>
    public interface IRepairRepository : IRepository<Szereles>
    {
    }
}
