﻿// <copyright file="IOrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using CarService.Data;

    /// <summary>
    /// Interface for order specific methods.
    /// OrderID is primary key. Remaining fields are foreign keys.
    /// It doesn't have methods, because it only contains primary and foreign keys and they cannot be updated.
    /// </summary>
    public interface IOrderRepository : IRepository<Megrendeles>
    {
    }
}
