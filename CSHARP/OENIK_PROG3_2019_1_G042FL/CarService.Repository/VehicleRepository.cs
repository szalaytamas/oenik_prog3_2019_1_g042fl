﻿// <copyright file="VehicleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Data;

    public class VehicleRepository : IVehicleRepository
    {
        private CarServiceDatabaseEntities carServiceDataEntities;

        public VehicleRepository(CarServiceDatabaseEntities carServiceDataEntities)
        {
            this.carServiceDataEntities = carServiceDataEntities;
        }

        /// <summary>
        /// Creates a new field in vehicle table.
        /// </summary>
        /// <param name="newData">Data to fill the new field.</param>
        public void Create(Gepjarmu newData)
        {
            this.carServiceDataEntities.Gepjarmu.Add(newData);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Delete a specified vehicle field of the table.
        /// </summary>
        /// <param name="dataToDelete">Specified object to delete from database</param>
        public void Delete(Gepjarmu dataToDelete)
        {
            this.carServiceDataEntities.Gepjarmu.Remove(dataToDelete);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Find the database element by its id.
        /// </summary>
        /// <param name="id">Id to find. id[0] VIN</param>
        /// <returns>Found database element, or null if there is nothing identified by id.</returns>
        public Gepjarmu GetById(string[] id)
        {
            if (this.carServiceDataEntities.Gepjarmu.Find(id[0]) != null)
            {
                return this.carServiceDataEntities.Gepjarmu.Find(id[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all data from the Vehicle table as a queryable collection.
        /// </summary>
        /// <returns>Queryable collection.</returns>
        public IQueryable<Gepjarmu> ReadAll()
        {
            return this.carServiceDataEntities.Gepjarmu;
        }

        /// <summary>
        /// Updates a field in vehicle table.
        /// </summary>
        /// <param name="dataGotFromLogic">First string in the array is the primary key of the table.
        /// Second is the column name to update.
        /// The following string is the new data to update.</param>
        public void Update(string[] dataGotFromLogic)
        {
            string vinId = dataGotFromLogic[0];
            string columnName = dataGotFromLogic[1];
            string dataToUpdate = dataGotFromLogic[2];

            switch (columnName)
            {
                case "manufacturer":
                    this.UpdateModel(vinId, dataToUpdate);
                    break;
                case "model":
                    this.UpdateMake(vinId, dataToUpdate);
                    break;
                case "body style":
                    this.UpdateBodyStyle(vinId, dataToUpdate);
                    break;
                case "color":
                    this.UpdateColor(vinId, dataToUpdate);
                    break;
                case "production date":
                    this.UpdateProductionDate(vinId, dataToUpdate);
                    break;
                default:

                    // The input column is not matched.
                    return;
            }
        }

        /// <summary>
        /// Update body style in vehicle table.
        /// </summary>
        /// <param name="vin">Primary key in vehicle table.</param>
        /// <param name="newBodyStyle">New body style to update.</param>
        public void UpdateBodyStyle(string vin, string newBodyStyle)
        {
            string[] id = { vin };
            this.GetById(id).Kivitel = newBodyStyle;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update color in vehicle table.
        /// </summary>
        /// <param name="vin">Primary key in vehicle table.</param>
        /// <param name="newColor">New color to update.</param>
        public void UpdateColor(string vin, string newColor)
        {
            string[] id = { vin };
            this.GetById(id).Szin = newColor;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update make in vehicle table. Modell in database.
        /// </summary>
        /// <param name="vin">Primary key in vehicle table.</param>
        /// <param name="newMake">New make to update.</param>
        public void UpdateMake(string vin, string newMake)
        {
            string[] id = { vin };
            this.GetById(id).Modell = newMake;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update model in vehicle table. Tipus in database.
        /// </summary>
        /// <param name="vin">Primary key in vehicle table.</param>
        /// <param name="newModel">New model to update.</param>
        public void UpdateModel(string vin, string newModel)
        {
            string[] id = { vin };
            this.GetById(id).Tipus = newModel;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update production date in vehicle table.
        /// </summary>
        /// <param name="vin">Primary key in vehicle table.</param>
        /// <param name="newProductionDate">New producton date to update.</param>
        public void UpdateProductionDate(string vin, string newProductionDate)
        {
            string[] id = { vin };
            this.GetById(id).Gyartasi_ev = int.Parse(newProductionDate);
            this.carServiceDataEntities.SaveChanges();
        }
    }
}
