﻿// <copyright file="IVehicleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using CarService.Data;

    /// <summary>
    /// Iterface for vehicle repository methods
    /// VIN - Vehicle Identification Number - primary key
    /// Primary and foreign keys cannot be updated.
    /// </summary>
    public interface IVehicleRepository : IRepository<Gepjarmu>
    {
        /// <summary>
        /// Central update method, Logic calls this first. Then this method calls the column specific update method.
        /// </summary>
        /// <param name="dataGotFromLogic">Data required to update.
        /// Not string like input parameter passed separately from string array.</param>
        void Update(string[] dataGotFromLogic);

        /// <summary>
        /// Make - Modell in databse e.g.: Honda
        /// </summary>
        /// <param name="vin">Vehicle Identification Number - primary key in Vehicle table.</param>
        /// <param name="newMake">New make to update.</param>
        void UpdateMake(string vin, string newMake);

        /// <summary>
        /// Model - Tipus in database e.g. C-RV
        /// </summary>
        /// <param name="vin">Vehicle Identification Number - primary key in Vehicle table.</param>
        /// <param name="newModel">New model to update.</param>
        void UpdateModel(string vin, string newModel);

        /// <summary>
        /// Body Style - Kivitel in database.
        /// </summary>
        /// <param name="vin">Vehicle Identification Number - primary key in Vehicle table.</param>
        /// <param name="newBodyStyle">New body style to update.</param>
        void UpdateBodyStyle(string vin, string newBodyStyle);

        /// <summary>
        /// Color - Szín in database.
        /// </summary>
        /// <param name="vin">Vehicle Identification Number - primary key in Vehicle table.</param>
        /// <param name="newColor">New color to update.</param>
        void UpdateColor(string vin, string newColor);

        /// <summary>
        /// Production date - gyártási év in database.
        /// </summary>
        /// <param name="vin">Vehicle Identification Number - primary key in Vehicle table.</param>
        /// <param name="newProductionDate">New production date to update.</param>
        void UpdateProductionDate(string vin, string newProductionDate);
    }
}
