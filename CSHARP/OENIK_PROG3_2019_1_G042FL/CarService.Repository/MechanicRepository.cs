﻿// <copyright file="MechanicRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Data;

    public class MechanicRepository : IMechanicRepository
    {
        private CarServiceDatabaseEntities carServiceDataEntities;

        public MechanicRepository(CarServiceDatabaseEntities carServiceDataEntities)
        {
            this.carServiceDataEntities = carServiceDataEntities;
        }

        /// <summary>
        /// Creates a new field in mechanic table.
        /// </summary>
        /// <param name="newData">Data to fill the new field.</param>
        public void Create(Szerelo newData)
        {
            this.carServiceDataEntities.Szerelo.Add(newData);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Delete a specified mechanic field of the table.
        /// </summary>
        /// <param name="dataToDelete">Specified object to delete from database</param>
        public void Delete(Szerelo dataToDelete)
        {
            this.carServiceDataEntities.Szerelo.Remove(dataToDelete);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Find the database element by its id
        /// </summary>
        /// <param name="id">Id to find. id[0] Mechanic_ID</param>
        /// <returns>Found database element, or null if there is nothing identified by id</returns>
        public Szerelo GetById(string[] id)
        {
            if (this.carServiceDataEntities.Szerelo.Find(int.Parse(id[0])) != null)
            {
                return this.carServiceDataEntities.Szerelo.Find(int.Parse(id[0]));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all data from the mechanic table as a queryable collection.
        /// </summary>
        /// <returns>Queryable collection.</returns>
        public IQueryable<Szerelo> ReadAll()
        {
            return this.carServiceDataEntities.Szerelo;
        }

        /// <summary>
        /// Updates a field in mechanic table.
        /// </summary>
        /// <param name="dataGotFromLogic">First string in the array is the primary key of the table.
        /// Second is the column name to update.
        /// The following string is the new data to update.</param>
        public void Update(string[] dataGotFromLogic)
        {
            string mechanicId = dataGotFromLogic[0];
            string columnName = dataGotFromLogic[1];
            string dataToUpdate = dataGotFromLogic[2];

                switch (columnName)
                {
                    case "mechanic's name":
                        this.UpdateName(mechanicId, dataToUpdate);
                        break;
                    case "qualification":
                        this.UpdateQualification(mechanicId, dataToUpdate);
                        break;
                    case "workshop address":
                        this.UpdateWorkshopAddress(mechanicId, dataToUpdate);
                        break;
                    default:
                        // The input column is not matched.
                        return;
                }
        }

        /// <summary>
        /// Updates mechanic name
        /// </summary>
        /// <param name="mechanicId">The primary key in mechanic table.</param>
        /// <param name="newName">The new name of mechanic</param>
        public void UpdateName(string mechanicId, string newName)
        {
            string[] id = { mechanicId };
            this.GetById(id).Nev = newName;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Updates mechanic qualification.
        /// </summary>
        /// <param name="mechanicId">The primary key in mechanic table.</param>
        /// <param name="newQualification">New qualification of mechanic.</param>
        public void UpdateQualification(string mechanicId, string newQualification)
        {
            string[] id = { mechanicId };
            this.GetById(id).Kepzettseg = newQualification;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update mechanic workshop address.
        /// </summary>
        /// <param name="mechanicId">The primary key of mechanic table.</param>
        /// <param name="newWorkshopAddress">New workshop address of mechanic.</param>
        public void UpdateWorkshopAddress(string mechanicId, string newWorkshopAddress)
        {
            string[] id = { mechanicId };
            this.GetById(id).Muhely_cime = newWorkshopAddress;
            this.carServiceDataEntities.SaveChanges();
        }
    }
}
