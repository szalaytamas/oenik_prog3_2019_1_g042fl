﻿// <copyright file="IMechanicRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using CarService.Data;

    /// <summary>
    /// Interface for mechanic methods.
    /// MechanicID is primary key.
    /// No foreign key.
    /// </summary>
    public interface IMechanicRepository : IRepository<Szerelo>
    {
        /// <summary>
        /// Central update method, Logic calls this first. Then this method calls the column specific update method.
        /// </summary>
        /// <param name="dataGotFromLogic">Data required to update.</param>
        void Update(string[] dataGotFromLogic);

        /// <summary>
        /// Update name field of mechanic table.
        /// </summary>
        /// <param name="mechanicId">Primary key of the field need to be updated.</param>
        /// <param name="newName">New name to update.</param>
        void UpdateName(string mechanicId, string newName);

        /// <summary>
        /// Update Qualification of mechanic table.
        /// </summary>
        /// <param name="mechanicId">Primary key of the field need to be updated.</param>
        /// <param name="newQualification">New qualification to update.</param>
        void UpdateQualification(string mechanicId, string newQualification);

        /// <summary>
        /// Update Workshop address of mechanic table.
        /// </summary>
        /// <param name="mechanicId">Primary key of the field need to be updated.</param>
        /// <param name="newWorkshopAddress">New workshop address to update.</param>
        void UpdateWorkshopAddress(string mechanicId, string newWorkshopAddress);
    }
}
