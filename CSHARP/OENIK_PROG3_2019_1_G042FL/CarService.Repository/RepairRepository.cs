﻿// <copyright file="RepairRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System;
    using System.Linq;
    using CarService.Data;

    public class RepairRepository : IRepairRepository
    {
        private CarServiceDatabaseEntities carServiceDataEntities;

        public RepairRepository(CarServiceDatabaseEntities carServiceDataEntities)
        {
            this.carServiceDataEntities = carServiceDataEntities;
        }

        /// <summary>
        /// Creates a new field in repair table.
        /// </summary>
        /// <param name="newData">Data to fill the new field.</param>
        public void Create(Szereles newData)
        {
            this.carServiceDataEntities.Szereles.Add(newData);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Delete a specified repair field of the table.
        /// </summary>
        /// <param name="dataToDelete">Specified object to delete from database</param>
        public void Delete(Szereles dataToDelete)
        {
            this.carServiceDataEntities.Szereles.Remove(dataToDelete);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Find the database element by its id
        /// </summary>
        /// <param name="id">Id to find. id[0] Work done. id[1] work date </param>
        /// <returns>Found database element, or null if there is nothing identified by id</returns>
        public Szereles GetById(string[] id)
        {
            DateTime date = DateTime.Now.Date;

            if (DateTime.TryParse(id[0], out date))
            {
                if (this.carServiceDataEntities.Szereles.Find(date) != null)
                {
                    return this.carServiceDataEntities.Szereles.Find(date);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all data from the Repair table as a queryable collection.
        /// </summary>
        /// <returns>Queryable collection.</returns>
        public IQueryable<Szereles> ReadAll()
        {
            return this.carServiceDataEntities.Szereles;
        }
    }
}
