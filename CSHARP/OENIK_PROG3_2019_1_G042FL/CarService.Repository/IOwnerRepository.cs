﻿// <copyright file="IOwnerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Data;

    /// <summary>
    /// Interface for owner methods
    /// Name and Telephone are primary keys
    /// </summary>
    public interface IOwnerRepository : IRepository<Tulajdonos>
    {
        /// <summary>
        /// Central update method, Logic calls this first. Then this method calls the column specific update method.
        /// </summary>
        /// <param name="dataGotFromLogic">Data required to update.</param>
        /// <param name="telephoneId">This telephoneId is a not string like input parameter passed separately from string array.</param>
        void Update(string[] dataGotFromLogic, int telephoneId);

        /// <summary>
        /// Updates the address field. Address - Cím in database.
        /// </summary>
        /// <param name="name">Name primary key.</param>
        /// <param name="telephone">Telephone primary key.</param>
        /// <param name="newAddress">New Address to update.</param>
        void UpdateAddress(string name, int telephone, string newAddress);

        /// <summary>
        /// Updates Email address field. Email Address - Email cím in database.
        /// </summary>
        /// <param name="name">Name primary key.</param>
        /// <param name="telephone">Telephone primary key.</param>
        /// <param name="newEmailAddress">New Email to update.</param>
        void UpdateEmailAddress(string name, int telephone, string newEmailAddress);
    }
}
