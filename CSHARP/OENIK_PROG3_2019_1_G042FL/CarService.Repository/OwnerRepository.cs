﻿// <copyright file="OwnerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarService.Data;

    public class OwnerRepository : IOwnerRepository
    {
        private CarServiceDatabaseEntities carServiceDataEntities;

        public OwnerRepository(CarServiceDatabaseEntities carServiceDataEntities)
        {
            this.carServiceDataEntities = carServiceDataEntities;
        }

        /// <summary>
        /// Creates a new field in Owner table.
        /// </summary>
        /// <param name="newData">Data to fill the new field.</param>
        public void Create(Tulajdonos newData)
        {
            this.carServiceDataEntities.Tulajdonos.Add(newData);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Delete a specified owner field of the table.
        /// </summary>
        /// <param name="dataToDelete">Specified object to delete from database</param>
        public void Delete(Tulajdonos dataToDelete)
        {
            this.carServiceDataEntities.Tulajdonos.Remove(dataToDelete);
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Find the database element by its id.
        /// </summary>
        /// <param name="id">Ids to find. id[0] Nev. id[1] Telefonszam (in 6701112233 form).</param>
        /// <returns>Found database element, or null if there is nothing identified by id</returns>
        public Tulajdonos GetById(string[] id)
        {
            object[] idObject = new object[2];
            idObject[0] = id[0];
            idObject[1] = int.Parse(id[1]);

            if (this.carServiceDataEntities.Tulajdonos.Find(idObject) != null)
            {
                return this.carServiceDataEntities.Tulajdonos.Find(idObject);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns all data from the Owner table as a queryable collection.
        /// </summary>
        /// <returns>Queryable collection.</returns>
        public IQueryable<Tulajdonos> ReadAll()
        {
            return this.carServiceDataEntities.Tulajdonos;
        }

        /// <summary>
        /// Updates a field in owner table.
        /// </summary>
        /// <param name="dataGotFromLogic">First string in the array is the primary key of the table.
        /// Second is the column name to update.
        /// The following string is the new data to update.</param>
        /// <param name="telephoneId"> The second primary key parsed to int.</param>
        public void Update(string[] dataGotFromLogic, int telephoneId)
        {
            string nameId = dataGotFromLogic[0];
            string columnName = dataGotFromLogic[1];
            string dataToUpdate = dataGotFromLogic[2];

            switch (columnName)
            {
                case "address":
                    this.UpdateAddress(nameId, telephoneId, dataToUpdate);
                    break;
                case "e-mail":
                    this.UpdateEmailAddress(nameId, telephoneId, dataToUpdate);
                    break;
                default:

                    // The input column is not matched.
                    return;
            }
        }

        /// <summary>
        /// Update adress in owner table.
        /// </summary>
        /// <param name="name">Primary key in owner table.</param>
        /// <param name="telephone">Second primary key in owner table.</param>
        /// <param name="newAddress">New address to update.</param>
        public void UpdateAddress(string name, int telephone, string newAddress)
        {
            string[] id = { name, telephone.ToString() };
            this.GetById(id).Cím = newAddress;
            this.carServiceDataEntities.SaveChanges();
        }

        /// <summary>
        /// Update email address in ower table.
        /// </summary>
        /// <param name="name">Primary key in owner table.</param>
        /// <param name="telephone">Second primary key in owner table.</param>
        /// <param name="newEmailAddress">New email address to update.</param>
        public void UpdateEmailAddress(string name, int telephone, string newEmailAddress)
        {
            string[] id = { name, telephone.ToString() };
            this.GetById(id).Email_cim = newEmailAddress;
            this.carServiceDataEntities.SaveChanges();
        }
    }
}
