﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for Logic methods
    /// Create
    /// Read
    /// Delete
    /// 3 more nonCRUD
    /// Java endpoint
    /// Getter methods for database description
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Reads out the table names from the database the logic has.
        /// </summary>
        /// <returns>Returns the table names from the database.</returns>
        string[] GetTableNames();

        /// <summary>
        /// Gets the coloumn names of a table.
        /// </summary>
        /// <param name="tableName">Table name to get its coloumns.</param>
        /// <returns>Returns the table column names as a string array</returns>
        string[] GetTableColumns(string tableName);

        /// <summary>
        /// Creates new field in a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// The following string(s) are the data to fill the new field.</param>
        void Create(string[] data);

        /// <summary>
        /// Updates a field in a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// Second string in the array is the primary key of the table.
        /// The following string(s) are the new data to update</param>
        void Update(string[] data);

        /// <summary>
        /// Updates a field in a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// Second string in the array is the primary key of the table.
        /// The following string(s) are the new data to update</param>
        void Delete(string[] data);

        /// <summary>
        /// Reads all data from a table, including coloumn names and give it back as formatted string.
        /// </summary>
        /// <param name="tableName">Table's name to read from.</param>
        /// <returns>Returns table data as a formatted string.</returns>
        string ReadAll(string tableName);

        /// <summary>
        /// Gets the coloumn names of a table which can be updated.
        /// </summary>
        /// <param name="tableName">Table name to get its coloumns.</param>
        /// <returns>Returns the table column names as a string array.</returns>
        string[] GetUpdatableColumnNames(string tableName);

        /// <summary>
        /// Gets the logic functions list.
        /// </summary>
        /// <returns>Returns as a string array.</returns>
        string[] GetLogicFunctions();

        /// <summary>
        /// Delivers java endpoint data.
        /// </summary>
        /// <param name="dataPassedToJava">Data passed to java operator method.</param>
        /// <returns>Returns the query result as a formatted string.</returns>
        string JavaEndpointQuery(int dataPassedToJava);

        /// <summary>
        /// Group mechanics by their qualification and counts them.
        /// </summary>
        /// <returns>Return the number of mechanics by qualification as a formatted string.</returns>
        string GroupMechanicsByQualification();

        /// <summary>
        /// Find a vehicle by its id(vehicle identification number) then find it's owner and give its email.
        /// </summary>
        /// <param name="vin">Vehicle identification number to find the vehicle.</param>
        /// <returns>Vehicle's make, VIN, owners name an email as a formatted string.</returns>
        string GetVehicleOwner(string vin);

        /// <summary>
        /// Gets a mechanic with its job done and its id.
        /// </summary>
        /// <param name="mechanicId">mechanic id to find</param>
        /// <returns>mechanic name, id, job done in a formatted string.</returns>
        string GetMechanicOfRepair(int mechanicId);
    }
}
