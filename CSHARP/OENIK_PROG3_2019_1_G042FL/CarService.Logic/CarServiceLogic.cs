﻿// <copyright file="CarServiceLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using CarService.JavaWeb;
    using Data;
    using Repository;

    /// <summary>
    /// Class for business logic methods.
    /// </summary>
    public class CarServiceLogic : ILogic
    {
        private readonly CarServiceDatabaseEntities carServiceDBEnt;

        private readonly IMechanicRepository mechanicRepo;
        private readonly IOrderRepository orderRepo;
        private readonly IOwnerRepository ownerRepo;
        private readonly IRepairRepository repairRepo;
        private readonly IVehicleRepository vehicleRepo;

        /// <summary>
        /// Store column names organized by table names.
        /// Key = table name as string.
        /// Value = column names as string[].
        /// </summary>
        private Dictionary<string, string[]> columnNamesByTables;

        /// <summary>
        /// Initializes a new instance of the <see cref="CarServiceLogic"/> class.
        /// Create a Database Entites instance.
        /// Create all repository instances.
        /// </summary>
        public CarServiceLogic()
        {
            this.carServiceDBEnt = new CarServiceDatabaseEntities();

            this.mechanicRepo = new MechanicRepository(this.carServiceDBEnt);
            this.orderRepo = new OrderRepository(this.carServiceDBEnt);
            this.ownerRepo = new OwnerRepository(this.carServiceDBEnt);
            this.repairRepo = new RepairRepository(this.carServiceDBEnt);
            this.vehicleRepo = new VehicleRepository(this.carServiceDBEnt);

            this.InitializeTableColumnNamesVariable();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CarServiceLogic"/> class.
        /// Parametered constructor for testing purposes.
        /// In test class library it gets mocked repositories.
        /// </summary>
        /// <param name="mechRep">mocked Mechanic Repository</param>
        /// <param name="ordeRep">mocked Order Repository</param>
        /// <param name="owneRep">mocked Owner Repository</param>
        /// <param name="repaRep">mocked Repair Repository</param>
        /// <param name="vehiRep">mocked Vehicle Repository</param>
        public CarServiceLogic(IMechanicRepository mechRep, IOrderRepository ordeRep, IOwnerRepository owneRep, IRepairRepository repaRep, IVehicleRepository vehiRep)
        {
            this.carServiceDBEnt = new CarServiceDatabaseEntities();

            this.mechanicRepo = mechRep;
            this.orderRepo = ordeRep;
            this.ownerRepo = owneRep;
            this.repairRepo = repaRep;
            this.vehicleRepo = vehiRep;

            this.InitializeTableColumnNamesVariable();
        }

        /// <summary>
        /// Creates new field in a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// The following string(s) are the data to fill the new field.</param>
        public void Create(string[] data)
        {
                switch (data[0])
                {
                    case "mechanic":
                        Szerelo szerelo = new Szerelo()
                        {
                            Szerelo_ID = this.TryParseForDatabaseIntegers(data[1]),
                            Nev = data[2],      // Mechanic's name
                            Kepzettseg = data[3],
                            Muhely_cime = data[4]
                        };

                        this.mechanicRepo.Create(szerelo);
                        break;

                    case "order":
                        Megrendeles megrendeles = new Megrendeles()
                        {
                            Megrendeles_ID = this.TryParseForDatabaseIntegers(data[1]),
                            Megrendeles_datum = this.TryParseForDatabaseDateTimes(data[2]),
                            Szerelo_ID = this.TryParseForDatabaseIntegers(data[3]),
                            Nev = data[4],
                            Telefonszam = this.TryParseForDatabaseIntegers(data[5])
                        };

                        this.orderRepo.Create(megrendeles);
                        break;

                    case "owner":
                        Tulajdonos tulajdonos = new Tulajdonos()
                        {
                            Nev = data[1],      // Ownre's name
                            Telefonszam = this.TryParseForDatabaseIntegers(data[2]),
                            Cím = data[3],
                            Email_cim = data[4]
                        };

                        this.ownerRepo.Create(tulajdonos);
                        break;

                    case "repair":
                        Szereles szereles = new Szereles()
                        {
                            Elvegzett_munka = data[1],
                            Datum = this.TryParseForDatabaseDateTimes(data[2]),
                            Alvazszam = data[3],
                            Szerelo_ID = this.TryParseForDatabaseIntegers(data[4])
                        };

                        this.repairRepo.Create(szereles);

                        break;

                    case "vehicle":
                        Gepjarmu gepjarmu = new Gepjarmu()
                        {
                            Alvazszam = data[1],
                            Modell = data[2],
                            Tipus = data[3],
                            Kivitel = data[4],
                            Szin = data[5],

                            // Something wrong with the database: production date is not a DateTime but an integer.
                            Gyartasi_ev = this.TryParseForDatabaseIntegers(data[6]),
                            Nev = data[7],
                            Telefonszam = this.TryParseForDatabaseIntegers(data[8]),
                            Szerelo_ID = this.TryParseForDatabaseIntegers(data[9])
                        };
                        this.vehicleRepo.Create(gepjarmu);

                        break;

                    default:

                        // If the input table not matched
                        return;
                }
        }

        /// <summary>
        /// Updates a field in a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// Second string in the array is the primary key of the table.
        /// Third is the column name to update, or the other primary key if it consists of two columns.
        /// The following string is the new data to update.</param>
        public void Update(string[] data)
        {
            string[] dataToPassRepo = { data[1], data[2], data[3] };

            switch (data[0])
            {
                case "mechanic":
                    this.mechanicRepo.Update(dataToPassRepo);
                    break;
                case "owner":
                    dataToPassRepo[2] = data[4];
                    this.ownerRepo.Update(dataToPassRepo, this.TryParseForDatabaseIntegers(data[3]));
                    break;
                case "vehicle":
                    this.vehicleRepo.Update(dataToPassRepo);
                    break;
                default:

                    // If the input table not matched
                    return;
            }
        }

        /// <summary>
        /// Deletes a field from a table.
        /// </summary>
        /// <param name="data">First string in the array is the table name.
        /// Second string in the array is the primary key of the table.</param>
        public void Delete(string[] data)
        {
            string[] dataToPassRepo = { data[1], data[2] };

            switch (data[0])
            {
                case "mechanic":
                    Szerelo szerelo = this.mechanicRepo.GetById(dataToPassRepo);
                    if (szerelo != null)
                    {
                        this.mechanicRepo.Delete(szerelo);
                    }

                    break;

                case "order":
                    Megrendeles megrendeles = this.orderRepo.GetById(dataToPassRepo);
                    if (megrendeles != null)
                    {
                        this.orderRepo.Delete(megrendeles);
                    }

                    break;

                case "owner":
                    Tulajdonos tulajdonos = this.ownerRepo.GetById(dataToPassRepo);
                    if (tulajdonos != null)
                    {
                        this.ownerRepo.Delete(tulajdonos);
                    }

                    break;

                case "repair":
                    Szereles szereles = this.repairRepo.GetById(dataToPassRepo);
                    if (szereles != null)
                    {
                        this.repairRepo.Delete(szereles);
                    }

                    break;

                case "vehicle":
                    Gepjarmu gepjarmu = this.vehicleRepo.GetById(dataToPassRepo);
                    if (gepjarmu != null)
                    {
                        this.vehicleRepo.Delete(gepjarmu);
                    }

                    break;

                default:

                    // If the input table not matched
                    return;
            }
        }

        /// <summary>
        /// Reads all data from a table, including coloumn names and give it back as formatted string.
        /// </summary>
        /// <param name="tableName">Table's name to read from.</param>
        /// <returns>Returns table data as a formatted string.</returns>
        public string ReadAll(string tableName)
        {
            string allData = string.Empty;

            // check if red table name is correct
            if (this.GetTableNames().Contains(tableName.ToLower()))
            {
                IQueryable<string> rawData;

                switch (tableName)
                {
                    case "mechanic":
                        rawData = this.mechanicRepo.ReadAll().Select(x =>
                            "### " +
                            x.Szerelo_ID + "\t" +
                            x.Nev + "\t" +
                            x.Kepzettseg + "\n" +
                            x.Muhely_cime);

                        allData = this.CreateFormattedStringForReadAll(rawData);
                        break;
                    case "order":
                        rawData = this.orderRepo.ReadAll().Select(x =>
                            "### " +
                            x.Megrendeles_ID + "\t" +
                            x.Megrendeles_datum + "\t" +
                            x.Szerelo_ID + "\t" +
                            x.Nev + "\t" +
                            x.Telefonszam);

                        allData = this.CreateFormattedStringForReadAll(rawData);
                        break;
                    case "owner":
                       rawData = this.ownerRepo.ReadAll().Select(x =>
                            "### " +
                            x.Nev + "\t" +
                            x.Telefonszam + "\n" +
                            x.Cím + "\t" +
                            x.Email_cim);

                        allData = this.CreateFormattedStringForReadAll(rawData);
                        break;
                    case "repair":
                        rawData = this.repairRepo.ReadAll().Select(x =>
                            "### " +
                            x.Elvegzett_munka + "\t" +
                            x.Datum + "\t" +
                            x.Alvazszam + "\t" +
                            x.Szerelo_ID);

                        allData = this.CreateFormattedStringForReadAll(rawData);
                        break;
                    case "vehicle":
                        rawData = this.vehicleRepo.ReadAll().Select(x =>
                            "### " +
                            x.Modell + "\t" +
                            x.Tipus + "\t" +
                            x.Kivitel + "\t" +
                            x.Szin + "\t" +
                            x.Alvazszam + "\n" +
                            x.Nev + "\t" +
                            x.Telefonszam + "\t" +
                            x.Szerelo_ID);

                        allData = this.CreateFormattedStringForReadAll(rawData);
                        break;
                }
            }
            else
            {
                allData = "Not valid table name.";
            }

            return allData;
        }

        /// <summary>
        /// Gets a mechanic with its job done and its id.
        /// </summary>
        /// <param name="mechanicId">mechanic id to find</param>
        /// <returns>mechanic name, id, job done in a formatted string.</returns>
        public string GetMechanicOfRepair(int mechanicId)
        {
            string result = string.Empty;

            var repairTable = this.repairRepo.ReadAll();
            var mechanicNamesAndIds = from mechanics in this.mechanicRepo.ReadAll()
                                      select new { mechanics.Nev, mechanics.Szerelo_ID };
            if (mechanicNamesAndIds != null)
            {
                var nameOfMechanic = mechanicNamesAndIds.Where(x => x.Szerelo_ID == mechanicId).Select(x => x.Nev).FirstOrDefault();
                result = nameOfMechanic;
                if (nameOfMechanic != null)
                {
                    var tempResult = (from repair in repairTable
                                      where repair.Szerelo_ID == mechanicId
                                      select new { ID = mechanicId, JobDone = repair.Elvegzett_munka }).First();

                    result += " " + tempResult.ID + " " + tempResult.JobDone;
                }
            }

            return result;
        }

        /// <summary>
        /// Find a vehicle by its id(vehicle identification number) then find it's owner and give its email.
        /// </summary>
        /// <param name="vin">Vehicle identification number to find the vehicle.</param>
        /// <returns>Vehicle's make, VIN, owners name an email as a formatted string.</returns>
        public string GetVehicleOwner(string vin)
        {
            string result = string.Empty;

            var vehicle = (from v in this.vehicleRepo.ReadAll()
                          where v.Alvazszam == vin
                          select new { v.Tipus, v.Alvazszam, v.Nev }).FirstOrDefault();

            if (vehicle != null)
            {
                result += vehicle.Tipus.ToString() + " " + vehicle.Alvazszam.ToString() + " " + vehicle.Nev.ToString();

                var ownerData = (from owner in this.ownerRepo.ReadAll()
                                 where owner.Nev == vehicle.Nev
                                 select owner.Email_cim).First();

                result += " " + ownerData.ToString();
            }

            return result;
        }

        /// <summary>
        /// Group mechanics by their qualification and counts them.
        /// </summary>
        /// <returns>Return the number of mechanics by qualification as a formatted string.</returns>
        public string GroupMechanicsByQualification()
        {
            string result = string.Empty;

            result = "qualifications\tnumber with this qualification\n";

            var qualifications = from m in this.mechanicRepo.ReadAll()
                    where m.Szerelo_ID < 10
                    group m by m.Kepzettseg into mByQual
                    select mByQual.Key;

            foreach (var item in qualifications)
            {
                var temp = (from q in this.mechanicRepo.ReadAll()
                            where q.Kepzettseg == item.ToString()
                            select q.Szerelo_ID).Count();

                result += item.ToString() + "\t" + temp.ToString() + "\n";
            }

            return result;
        }

        /// <summary>
        /// Delivers java endpoint data.
        /// </summary>
        /// <param name="dataPassedToJava">Data passed to java operator method. Can be 0, 1, 2</param>
        /// <returns>Returns the query result as a formatted string.</returns>
        public string JavaEndpointQuery(int dataPassedToJava)
        {
            string formattedString = string.Empty;
            Java java = new Java();

            formattedString = java.GetDataJavaEndpoint(dataPassedToJava);

            return formattedString;
        }

        /// <summary>
        /// Gets the coloumn names of a table which can be updated.
        /// </summary>
        /// <param name="tableName">Table name to get its coloumns.</param>
        /// <returns>Returns the table column names as a string array.</returns>
        public string[] GetUpdatableColumnNames(string tableName)
        {
            Dictionary<string, string[]> columns;

            string[] mechanicColumnsUpdate = { "mechanic's name", "qualification", "workshop address" };
            string[] ownerColumnsUpdate = { "address", "e-mail" };
            string[] vehicleColumnsUpdate = { "manufacturer", "model", "body style", "color", "production date" };

            columns = new Dictionary<string, string[]>();

            columns.Add("mechanic", mechanicColumnsUpdate);
            columns.Add("owner", ownerColumnsUpdate);
            columns.Add("vehicle", vehicleColumnsUpdate);

            return columns[tableName];
        }

        /// <summary>
        /// Gets the coloumn names of a table.
        /// </summary>
        /// <param name="tableName">Table name to get its coloumns.</param>
        /// <returns>Returns the table column names as a string array</returns>
        public string[] GetTableColumns(string tableName)
        {
            string[] columnNames = this.columnNamesByTables[tableName.ToLower()];

            return columnNames;
        }

        /// <summary>
        /// Get the tables name from the database.
        /// This case it's a constant and they are given here.
        /// </summary>
        /// <returns>String array of table names of the database.</returns>
        public string[] GetTableNames()
        {
            string[] tables = { "mechanic", "order", "owner", "repair", "vehicle" };

            return tables;
        }

        /// <summary>
        /// Gets the logic functions list.
        /// </summary>
        /// <returns>Returns as a string array.</returns>
        public string[] GetLogicFunctions()
        {
            string[] functions = { "create", "read", "update", "delete", "get the mechanic of a repair", "get owner of a vehicle", "mechanics grouped by qualification", "get a special last minute offer (JAVA)" };

            return functions;
        }

        /// <summary>
        /// Get the table column names from the database.
        /// Store them in a dictionary, where table names are the keys and column names are the values.
        /// This case it's a constant and they are given here.
        /// </summary>
        private void InitializeTableColumnNamesVariable()
        {
            string[] mechanicColumns = { "mechanic ID", "mechanic's name", "qualification", "workshop address" };
            string[] orderColumns = { "order ID", "order date", "mechanic ID", "owner's name", "owner's telephone" };
            string[] ownerColumns = { "name", "telephone", "address", "e-mail" };
            string[] repairColumns = { "work done", "work date", "vehicle identification number", "mechanic ID" };
            string[] vehicleColumns = { "vehicle identification number", "manufacturer", "model", "body style", "color", "production date", "owner's name", "owner's telephone", "mechanic ID" };

            this.columnNamesByTables = new Dictionary<string, string[]>();

            this.columnNamesByTables.Add("mechanic", mechanicColumns);
            this.columnNamesByTables.Add("order", orderColumns);
            this.columnNamesByTables.Add("owner", ownerColumns);
            this.columnNamesByTables.Add("repair", repairColumns);
            this.columnNamesByTables.Add("vehicle", vehicleColumns);
        }

        /// <summary>
        /// Creates a formatted output string from ReadAll method queryable return
        /// </summary>
        /// <param name="input">Queryable collection</param>
        /// <returns>Single formatted string contains all th required data.</returns>
        private string CreateFormattedStringForReadAll(IQueryable<string> input)
        {
            string output = string.Empty;

            output += "+------------------------------------------+\n";
            foreach (var item in input)
            {
                output += item + "\n";
            }

            output += "+------------------------------------------+\n";

            return output;
        }

        /// <summary>
        /// Uses the bultin int.TryParse method and also trims too big numbers
        /// </summary>
        /// <param name="stringToParse">Input string to parse</param>
        /// <returns>Int32 value parsed from the input string or 0 if it cannot be parsed.</returns>
        private int TryParseForDatabaseIntegers(string stringToParse)
        {
            int value;

            // Trims off the characters after the 9th.
            if (stringToParse.Length >= 9)
            {
                stringToParse = stringToParse.Remove(9, stringToParse.Length - 9);
            }

            if (int.TryParse(stringToParse, out value))
            {
                return value;
            }

            return 0;
        }

        /// <summary>
        /// Uses the bultin DateTime.TryParse method to parse strings to date if it possible.
        /// </summary>
        /// <param name="stringToParse">String to parse.</param>
        /// <returns>Returns the Date in DateTime form or creation date if input was not in a correct form.</returns>
        private DateTime TryParseForDatabaseDateTimes(string stringToParse)
        {
            DateTime date = DateTime.Now.Date;

            if (DateTime.TryParse(stringToParse, out date))
            {
                return date.Date;
            }

            return date;
        }
    }
}
