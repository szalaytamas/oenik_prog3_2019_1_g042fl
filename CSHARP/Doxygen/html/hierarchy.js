var hierarchy =
[
    [ "CarService.Logic.Tests.CarServiceTests", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html", null ],
    [ "DbContext", null, [
      [ "CarService.Data.CarServiceDatabaseEntities", "class_car_service_1_1_data_1_1_car_service_database_entities.html", null ]
    ] ],
    [ "CarService.Data.Gepjarmu", "class_car_service_1_1_data_1_1_gepjarmu.html", null ],
    [ "CarService.JavaWeb.IJava", "interface_car_service_1_1_java_web_1_1_i_java.html", [
      [ "CarService.JavaWeb.Java", "class_car_service_1_1_java_web_1_1_java.html", null ]
    ] ],
    [ "CarService.Logic.ILogic", "interface_car_service_1_1_logic_1_1_i_logic.html", [
      [ "CarService.Logic.CarServiceLogic", "class_car_service_1_1_logic_1_1_car_service_logic.html", null ]
    ] ],
    [ "OENIK_PROG3_2019_1_G042FL.IMainMenu", "interface_o_e_n_i_k___p_r_o_g3__2019__1___g042_f_l_1_1_i_main_menu.html", [
      [ "OENIK_PROG3_2019_1_G042FL.MainMenu", "class_o_e_n_i_k___p_r_o_g3__2019__1___g042_f_l_1_1_main_menu.html", null ]
    ] ],
    [ "CarService.Repository.IRepository< T >", "interface_car_service_1_1_repository_1_1_i_repository.html", null ],
    [ "CarService.Repository.IRepository< Gepjarmu >", "interface_car_service_1_1_repository_1_1_i_repository.html", [
      [ "CarService.Repository.IVehicleRepository", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html", [
        [ "CarService.Repository.VehicleRepository", "class_car_service_1_1_repository_1_1_vehicle_repository.html", null ]
      ] ]
    ] ],
    [ "CarService.Repository.IRepository< Megrendeles >", "interface_car_service_1_1_repository_1_1_i_repository.html", [
      [ "CarService.Repository.IOrderRepository", "interface_car_service_1_1_repository_1_1_i_order_repository.html", [
        [ "CarService.Repository.OrderRepository", "class_car_service_1_1_repository_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "CarService.Repository.IRepository< Szereles >", "interface_car_service_1_1_repository_1_1_i_repository.html", [
      [ "CarService.Repository.IRepairRepository", "interface_car_service_1_1_repository_1_1_i_repair_repository.html", [
        [ "CarService.Repository.RepairRepository", "class_car_service_1_1_repository_1_1_repair_repository.html", null ]
      ] ]
    ] ],
    [ "CarService.Repository.IRepository< Szerelo >", "interface_car_service_1_1_repository_1_1_i_repository.html", [
      [ "CarService.Repository.IMechanicRepository", "interface_car_service_1_1_repository_1_1_i_mechanic_repository.html", [
        [ "CarService.Repository.MechanicRepository", "class_car_service_1_1_repository_1_1_mechanic_repository.html", null ]
      ] ]
    ] ],
    [ "CarService.Repository.IRepository< Tulajdonos >", "interface_car_service_1_1_repository_1_1_i_repository.html", [
      [ "CarService.Repository.IOwnerRepository", "interface_car_service_1_1_repository_1_1_i_owner_repository.html", [
        [ "CarService.Repository.OwnerRepository", "class_car_service_1_1_repository_1_1_owner_repository.html", null ]
      ] ]
    ] ],
    [ "CarService.Data.Megrendeles", "class_car_service_1_1_data_1_1_megrendeles.html", null ],
    [ "OENIK_PROG3_2019_1_G042FL.Program", "class_o_e_n_i_k___p_r_o_g3__2019__1___g042_f_l_1_1_program.html", null ],
    [ "CarService.Data.Szereles", "class_car_service_1_1_data_1_1_szereles.html", null ],
    [ "CarService.Data.Szerelo", "class_car_service_1_1_data_1_1_szerelo.html", null ],
    [ "CarService.Data.Tulajdonos", "class_car_service_1_1_data_1_1_tulajdonos.html", null ]
];