var class_car_service_1_1_logic_1_1_car_service_logic =
[
    [ "CarServiceLogic", "class_car_service_1_1_logic_1_1_car_service_logic.html#a37b7938624c5e0e8a6a5c0b53aa1b829", null ],
    [ "CarServiceLogic", "class_car_service_1_1_logic_1_1_car_service_logic.html#ada0a8ae3eb8f459cd8eb82870f9034f9", null ],
    [ "Create", "class_car_service_1_1_logic_1_1_car_service_logic.html#a7d8c57ac16b5373dba398340deacb29d", null ],
    [ "Delete", "class_car_service_1_1_logic_1_1_car_service_logic.html#a5e28914bf02648e0eee28b30f2b39b03", null ],
    [ "GetLogicFunctions", "class_car_service_1_1_logic_1_1_car_service_logic.html#a265e8e199c64c6d05f44aae92dec4bc4", null ],
    [ "GetMechanicOfRepair", "class_car_service_1_1_logic_1_1_car_service_logic.html#af9ed73d82012e480d48a1188a8c47d07", null ],
    [ "GetTableColumns", "class_car_service_1_1_logic_1_1_car_service_logic.html#a55af46be9fa88e71a95e505d8e9bdc7f", null ],
    [ "GetTableNames", "class_car_service_1_1_logic_1_1_car_service_logic.html#ad7cf9fde103baef9d8c2977107397c72", null ],
    [ "GetUpdatableColumnNames", "class_car_service_1_1_logic_1_1_car_service_logic.html#aa82fac1cf65be219bced9fe5b501dd60", null ],
    [ "GetVehicleOwner", "class_car_service_1_1_logic_1_1_car_service_logic.html#a299de9d030c9331652ec8d9936b29cc1", null ],
    [ "GroupMechanicsByQualification", "class_car_service_1_1_logic_1_1_car_service_logic.html#af742ca1d41beba5231963c1461380e6b", null ],
    [ "JavaEndpointQuery", "class_car_service_1_1_logic_1_1_car_service_logic.html#a3249e3a2635fa560e39a9024d3519c25", null ],
    [ "ReadAll", "class_car_service_1_1_logic_1_1_car_service_logic.html#a29f2734c5d15decdc81bc8d68b28c50e", null ],
    [ "Update", "class_car_service_1_1_logic_1_1_car_service_logic.html#ab072fa8ac62b9c4460e0caff7d9e4904", null ]
];