/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "oenik_prog3_2019_1_g042fl", "index.html", [
    [ "Castle Core Changelog", "md__f_1_a_egyetemi_cucc_4_8f_xC3_xA9l_xC3_xA9v_whp_nhf__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_1_556ffbb63895460664bc2e387ce5f73a.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__f_1_a_egyetemi_cucc_4_8f_xC3_xA9l_xC3_xA9v_whp_nhf__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_1_40a4c41ca2c08e7e8aa84a0d23887225.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_car_service_8_data_2_properties_2_assembly_info_8cs_source.html",
"interface_car_service_1_1_repository_1_1_i_vehicle_repository.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';