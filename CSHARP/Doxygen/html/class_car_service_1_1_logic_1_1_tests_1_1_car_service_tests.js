var class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests =
[
    [ "CheckIfCreateMethodIsCalled", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a34ac8c3f0f6a0ada02fa78a860877d58", null ],
    [ "CheckIfDeleteMethodIsCalled", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a6f8fd236b9f30fcd232d337a15fb67f1", null ],
    [ "CheckIfReturnedTableColumnsCorrect", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#ada06ac8b7598e6f29020a36a16e3e9cd", null ],
    [ "CheckIfReturnedTableNamesCorrect", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a2a1289b3d15cdbce622437ad3641af2e", null ],
    [ "CheckIfReturnEmptyOnNotValidId", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#abd2d33e1454c977e9c7bf3c9485d47e6", null ],
    [ "CheckIfReturnEmptyOnWrongId", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a5c1a0306079f98da874202fc1387ea27", null ],
    [ "CheckIfReturnMechanicOnValidId", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#aef713e893f1356b0f33a7a6561c2d453", null ],
    [ "CheckIfReturnNotNullOnNoNEmptyTable", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a079eebc29254c6c5857fea780b1023d4", null ],
    [ "CheckIfReturnOwnerOnValidId", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#ad8516b474b12bab097289b52bf630d89", null ],
    [ "CheckIfReturnTypeIsString", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a48a74920c9125542cd18de60d3dadb0a", null ],
    [ "CheckIfUpdateMethodIsCalled", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a1fe9f15e172c5d8e3c569eaf16b23590", null ],
    [ "NotThrowOnWrongDataCreate", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#aaa5b0cd0c84523388a4174725664086a", null ],
    [ "ReadallGivesMessageOnWrongTableName", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a958320e5e0419c38a13d4dfbabc80ed6", null ],
    [ "ReadallReturnTypeIsAString", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a56a836bf9ef2c96e5eedab233a227b99", null ],
    [ "Setup", "class_car_service_1_1_logic_1_1_tests_1_1_car_service_tests.html#a8fd4312408eafaacdd862d82016ba39e", null ]
];