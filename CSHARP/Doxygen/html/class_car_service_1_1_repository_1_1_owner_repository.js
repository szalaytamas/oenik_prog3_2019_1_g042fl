var class_car_service_1_1_repository_1_1_owner_repository =
[
    [ "OwnerRepository", "class_car_service_1_1_repository_1_1_owner_repository.html#a5f53a9ac7a10d7708e3a3ab4ce655e02", null ],
    [ "Create", "class_car_service_1_1_repository_1_1_owner_repository.html#a0e6ce715cdb4e735916cdd2eea66a691", null ],
    [ "Delete", "class_car_service_1_1_repository_1_1_owner_repository.html#ac8dab54f7b59aa32e0e478d457174160", null ],
    [ "GetById", "class_car_service_1_1_repository_1_1_owner_repository.html#a810b58b9851aaee86a11542a1ec1c75f", null ],
    [ "ReadAll", "class_car_service_1_1_repository_1_1_owner_repository.html#abd3b66a20f73b7a522ba0f1190d1843e", null ],
    [ "Update", "class_car_service_1_1_repository_1_1_owner_repository.html#ae0c67382c3f072331e7cc353429e70b8", null ],
    [ "UpdateAddress", "class_car_service_1_1_repository_1_1_owner_repository.html#aade160ad0430cb85c293dac9b7a244ba", null ],
    [ "UpdateEmailAddress", "class_car_service_1_1_repository_1_1_owner_repository.html#aaa96e77fb611cc3eaaa413a097b0c9d7", null ]
];