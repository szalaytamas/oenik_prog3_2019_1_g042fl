var interface_car_service_1_1_repository_1_1_i_vehicle_repository =
[
    [ "Update", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#ad426137b6614e6d5465641cc95d7d4cc", null ],
    [ "UpdateBodyStyle", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#adffda8325941b6010c77ccc7836da640", null ],
    [ "UpdateColor", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#ab321b04e597577fda6cb34a8a36709b5", null ],
    [ "UpdateMake", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#ac34b7e802d0acfd530c6fff0d123c9f4", null ],
    [ "UpdateModel", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#ac311f0d4cb8217874ae80af07381ba01", null ],
    [ "UpdateProductionDate", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html#aa01cccac354defc77e1bf86db354e993", null ]
];