var interface_car_service_1_1_logic_1_1_i_logic =
[
    [ "Create", "interface_car_service_1_1_logic_1_1_i_logic.html#a04645d03c8186ffe67af0f3267ccc24e", null ],
    [ "Delete", "interface_car_service_1_1_logic_1_1_i_logic.html#a21d76d1b27d35f58a625c7604d4696e3", null ],
    [ "GetLogicFunctions", "interface_car_service_1_1_logic_1_1_i_logic.html#a000384c348c0121867a4bb283fe6a197", null ],
    [ "GetMechanicOfRepair", "interface_car_service_1_1_logic_1_1_i_logic.html#a288899823086726beed68fc709b486d5", null ],
    [ "GetTableColumns", "interface_car_service_1_1_logic_1_1_i_logic.html#a16fb52ea1e390a9490951c278995f5b4", null ],
    [ "GetTableNames", "interface_car_service_1_1_logic_1_1_i_logic.html#a6e6046546c17605254ce97a7a74b57e7", null ],
    [ "GetUpdatableColumnNames", "interface_car_service_1_1_logic_1_1_i_logic.html#a68d8939cd35fb32ae60ad7bad4f43b34", null ],
    [ "GetVehicleOwner", "interface_car_service_1_1_logic_1_1_i_logic.html#addb45ecb5e6c204f84d5f9a4bd78e535", null ],
    [ "GroupMechanicsByQualification", "interface_car_service_1_1_logic_1_1_i_logic.html#a723d08b65f4609f72bf695a4c6bfcbaf", null ],
    [ "JavaEndpointQuery", "interface_car_service_1_1_logic_1_1_i_logic.html#a95baf01166b5309a982b2ee515f38883", null ],
    [ "ReadAll", "interface_car_service_1_1_logic_1_1_i_logic.html#a39b0a0047ea5fa1c456b9f36c32023eb", null ],
    [ "Update", "interface_car_service_1_1_logic_1_1_i_logic.html#a46fc10c19ed07ed8356980185f2cc934", null ]
];