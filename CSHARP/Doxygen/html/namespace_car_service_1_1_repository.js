var namespace_car_service_1_1_repository =
[
    [ "IMechanicRepository", "interface_car_service_1_1_repository_1_1_i_mechanic_repository.html", "interface_car_service_1_1_repository_1_1_i_mechanic_repository" ],
    [ "IOrderRepository", "interface_car_service_1_1_repository_1_1_i_order_repository.html", null ],
    [ "IOwnerRepository", "interface_car_service_1_1_repository_1_1_i_owner_repository.html", "interface_car_service_1_1_repository_1_1_i_owner_repository" ],
    [ "IRepairRepository", "interface_car_service_1_1_repository_1_1_i_repair_repository.html", null ],
    [ "IRepository", "interface_car_service_1_1_repository_1_1_i_repository.html", "interface_car_service_1_1_repository_1_1_i_repository" ],
    [ "IVehicleRepository", "interface_car_service_1_1_repository_1_1_i_vehicle_repository.html", "interface_car_service_1_1_repository_1_1_i_vehicle_repository" ],
    [ "MechanicRepository", "class_car_service_1_1_repository_1_1_mechanic_repository.html", "class_car_service_1_1_repository_1_1_mechanic_repository" ],
    [ "OrderRepository", "class_car_service_1_1_repository_1_1_order_repository.html", "class_car_service_1_1_repository_1_1_order_repository" ],
    [ "OwnerRepository", "class_car_service_1_1_repository_1_1_owner_repository.html", "class_car_service_1_1_repository_1_1_owner_repository" ],
    [ "RepairRepository", "class_car_service_1_1_repository_1_1_repair_repository.html", "class_car_service_1_1_repository_1_1_repair_repository" ],
    [ "VehicleRepository", "class_car_service_1_1_repository_1_1_vehicle_repository.html", "class_car_service_1_1_repository_1_1_vehicle_repository" ]
];