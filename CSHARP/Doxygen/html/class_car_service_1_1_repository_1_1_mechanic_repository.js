var class_car_service_1_1_repository_1_1_mechanic_repository =
[
    [ "MechanicRepository", "class_car_service_1_1_repository_1_1_mechanic_repository.html#ac3eeb76ccdba4159075432156ffd0d29", null ],
    [ "Create", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a1a396e90dac3ea17c14a98ac7f5ac9ab", null ],
    [ "Delete", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a285a4ac0c3683200d75b6c4ed030a50e", null ],
    [ "GetById", "class_car_service_1_1_repository_1_1_mechanic_repository.html#af47e9f957602f15a02f4ce9a83bdba4e", null ],
    [ "ReadAll", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a8797a59346cca896dac7506faf0c014e", null ],
    [ "Update", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a0e22786a99787fe63b7911f280aa8d1e", null ],
    [ "UpdateName", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a2edecd54b3b48cf3cb4cdebbc13a70ba", null ],
    [ "UpdateQualification", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a5f50effc0fc8f01d7012181494dda4dd", null ],
    [ "UpdateWorkshopAddress", "class_car_service_1_1_repository_1_1_mechanic_repository.html#a16bc4db680586384bc667ec2fca41941", null ]
];