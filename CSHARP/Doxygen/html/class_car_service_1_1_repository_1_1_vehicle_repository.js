var class_car_service_1_1_repository_1_1_vehicle_repository =
[
    [ "VehicleRepository", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a6ca656b5a18bf779871814348002bd66", null ],
    [ "Create", "class_car_service_1_1_repository_1_1_vehicle_repository.html#ab9b67310a45df2a401a9a7af6dce084e", null ],
    [ "Delete", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a9c393c18ee51f000ebfd7ef58c44eaee", null ],
    [ "GetById", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a89d9f5e6d847089e606b53e5cb5108ae", null ],
    [ "ReadAll", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a0868dc6cb07ac4656a745eff36a0ace7", null ],
    [ "Update", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a7240e536ed185b6a99fdc57e3d047b25", null ],
    [ "UpdateBodyStyle", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a572d216ea631f39c476f65a2cf0a9baf", null ],
    [ "UpdateColor", "class_car_service_1_1_repository_1_1_vehicle_repository.html#ae4cedbfeeb0b395ab76d9c4894b6e223", null ],
    [ "UpdateMake", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a823b8736f12a8d0a3429553720f08f51", null ],
    [ "UpdateModel", "class_car_service_1_1_repository_1_1_vehicle_repository.html#ae43b8346ab26b672ef5df775e4a919fe", null ],
    [ "UpdateProductionDate", "class_car_service_1_1_repository_1_1_vehicle_repository.html#a54f0e25579f47f59a9b5a01f14c8f3a6", null ]
];