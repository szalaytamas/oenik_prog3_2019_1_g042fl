var searchData=
[
  ['ijava',['IJava',['../interface_car_service_1_1_java_web_1_1_i_java.html',1,'CarService::JavaWeb']]],
  ['ilogic',['ILogic',['../interface_car_service_1_1_logic_1_1_i_logic.html',1,'CarService::Logic']]],
  ['imainmenu',['IMainMenu',['../interface_o_e_n_i_k___p_r_o_g3__2019__1___g042_f_l_1_1_i_main_menu.html',1,'OENIK_PROG3_2019_1_G042FL']]],
  ['imechanicrepository',['IMechanicRepository',['../interface_car_service_1_1_repository_1_1_i_mechanic_repository.html',1,'CarService::Repository']]],
  ['iorderrepository',['IOrderRepository',['../interface_car_service_1_1_repository_1_1_i_order_repository.html',1,'CarService::Repository']]],
  ['iownerrepository',['IOwnerRepository',['../interface_car_service_1_1_repository_1_1_i_owner_repository.html',1,'CarService::Repository']]],
  ['irepairrepository',['IRepairRepository',['../interface_car_service_1_1_repository_1_1_i_repair_repository.html',1,'CarService::Repository']]],
  ['irepository',['IRepository',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['irepository_3c_20gepjarmu_20_3e',['IRepository&lt; Gepjarmu &gt;',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['irepository_3c_20megrendeles_20_3e',['IRepository&lt; Megrendeles &gt;',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['irepository_3c_20szereles_20_3e',['IRepository&lt; Szereles &gt;',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['irepository_3c_20szerelo_20_3e',['IRepository&lt; Szerelo &gt;',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['irepository_3c_20tulajdonos_20_3e',['IRepository&lt; Tulajdonos &gt;',['../interface_car_service_1_1_repository_1_1_i_repository.html',1,'CarService::Repository']]],
  ['ivehiclerepository',['IVehicleRepository',['../interface_car_service_1_1_repository_1_1_i_vehicle_repository.html',1,'CarService::Repository']]]
];
