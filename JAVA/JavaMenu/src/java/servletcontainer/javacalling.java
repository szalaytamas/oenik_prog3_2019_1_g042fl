/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servletcontainer;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author M
 */
public class javacalling extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String myVariable = request.getParameter("myVariable");
        
        int number=Integer.parseInt(myVariable);
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if (number==0) {
                out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            out.println("<autos>"); 
            out.println("<names>"+"Hyundai Terracan"+"</names>");
            out.println("<dealers>"+"Ford Petrányi"+"</dealers>");
            out.println("<prices>"+"10M HUF"+"</prices>");
            out.println("</autos>");
            }
            if (number==1) {
                out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            out.println("<autos>"); 
            out.println("<names>"+"Mazda 6"+"</names>");
            out.println("<dealers>"+"Peugeot Gablini M3"+"</dealers>");
            out.println("<prices>"+"10M HUF"+"</prices>");
            out.println("</autos>");
            }
            if (number==2) {
                out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            out.println("<autos>"); 
            out.println("<names>"+"BMW"+"</names>");
            out.println("<dealers>"+"ZiziCar"+"</dealers>");
            out.println("<prices>"+"1 HUF"+"</prices>");
            out.println("</autos>");
            }
                    
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
